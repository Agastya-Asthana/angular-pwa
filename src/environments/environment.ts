// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCVLniIOLhADuvSiFmMly4RTceLzphibUY",
    authDomain: "angular-firestarter-4f2c2.firebaseapp.com",
    databaseURL: "https://angular-firestarter-4f2c2.firebaseio.com",
    projectId: "angular-firestarter-4f2c2",
    storageBucket: "angular-firestarter-4f2c2.appspot.com",
    messagingSenderId: "678280216835",
    appId: "1:678280216835:web:025cf6a896414309b6a936",
    measurementId: "G-YR9FXMPW0D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
